# DROP TABLE bookmanager.USER;
# DROP TABLE bookmanager.ROLE;

# CREATE TABLE test.BOOK (
#   ID INT NOT NULL AUTO_INCREMENT,
#   BOOK_TITLE VARCHAR(255) NOT NULL,
#   BOOK_AUTHOR VARCHAR(255) NOT NULL,
#   BOOK_PRICE INT NOT NULL,
#   PRIMARY KEY (ID));

# INSERT INTO test.regions (region_id, region) VALUES ('2', 'Белгород');


# INSERT INTO test.organizations (organizations_id, region_id, organization) VALUES ('1', '1', 'Брянская мясная компания');
# INSERT INTO test.organizations (organizations_id, region_id, organization) VALUES ('2', '1', 'Брянский бройлер');


# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('1', '1', 'Брасовское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('2', '1', 'Климовское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('3', '1', 'Мглинское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('4', '1', 'Почепское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('5', '1', 'Рогнеденское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('6', '1', 'Трубчевское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('7', '1', 'Калужское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('8', '1', 'Орловское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('9', '1', 'Смоленское подразделение');
# INSERT INTO test.subdivisions (subdivision_id, organization_id, subdivision) VALUES ('10', '1', 'Тульское подразделение');


# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('1', '1', 'Кретово');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('2', '1', 'Куприно');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('3', '1', 'Мариничи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('4', '1', 'Осотское');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('5', '2', 'Азаровка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('6', '2', 'Воронок');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('7', '2', 'Крапивна');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('8', '2', 'Куршановичи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('9', '2', 'Любечане');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('10', '2', 'Хоромное');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('11', '3', 'Большая Ловча');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('12', '3', 'Ветлевка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('13', '3', 'Вормино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('14', '3', 'Высокоселище');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('15', '3', 'Коржовка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('16', '3', 'Красновичи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('17', '3', 'Красные Косары');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('18', '3', 'Ляличи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('19', '3', 'Старая Романовка');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('20', '4', 'Акуличи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('21', '4', 'Березовка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('22', '4', 'Валуец');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('23', '4', 'Житня');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('24', '4', 'Норино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('25', '4', 'Первомайское');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('26', '4', 'Рубча');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('27', '4', 'Савостьяны');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('28', '4', 'Староселье');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('29', '4', 'Супрягино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('30', '4', 'Щекотово');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('31', '5', 'Алешня');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('32', '5', 'Леденево');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('33', '5', 'Любышь');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('34', '5', 'Мареевка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('35', '5', 'Радичи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('36', '5', 'Ратовское');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('37', '5', 'Селиловичи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('38', '5', 'Тюнино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('39', '5', 'Ходиловичи');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('40', '6', 'Глыбочка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('41', '6', 'Комягино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('42', '6', 'Котляково');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('43', '6', 'Плюсково');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('44', '6', 'Ужа');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('45', '7', 'Алексеевка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('46', '7', 'Бережки');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('47', '7', 'Клевенево');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('48', '7', 'Лужница');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('49', '7', 'Наумово');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('50', '7', 'Полом');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('51', '7', 'Романьково');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('52', '7', 'Стайки');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('53', '7', 'Татаринцы');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('54', '7', 'Фроловка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('55', '7', 'Холмовая');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('56', '8', 'Работьково');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('57', '8', 'Фатнево');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('58', '8', 'Юшково');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('59', '9', 'Андреевка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('60', '9', 'Ворошилово');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('61', '9', 'Думаничи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('62', '9', 'Зарево');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('63', '9', 'Иозефовка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('64', '9', 'Лобановка');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('65', '9', 'Любавичи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('66', '9', 'Новая Рудня');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('67', '9', 'Пригорье');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('68', '9', 'Рухань');
#
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('69', '10', 'Араны');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('70', '10', 'Белолипки');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('71', '10', 'Будоговищи');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('72', '10', 'Воскресенское');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('73', '10', 'Жуково');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('74', '10', 'Красный Яр');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('75', '10', 'Ленино');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('76', '10', 'Нестерово');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('77', '10', 'Пришня');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('78', '10', 'Синегубово');
# INSERT INTO test.objects (object_id, subdivision_id, object) VALUES ('79', '10', 'Шишкино');



# INSERT INTO test.rooms (room_id, room) VALUES ('1', 'АБК');
# INSERT INTO test.rooms (room_id, room) VALUES ('2', 'Вет. аптека');
# INSERT INTO test.rooms (room_id, room) VALUES ('3', 'Прачечная');
# INSERT INTO test.rooms (room_id, room) VALUES ('4', 'Кухня');
# INSERT INTO test.rooms (room_id, room) VALUES ('5', 'Внешняя территория');
# INSERT INTO test.rooms (room_id, room) VALUES ('6', 'Серверная');


# TRUNCATE TABLE rooms_objects;
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('1', '1', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('2', '1', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('3', '1', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('4', '1', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('5', '1', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('6', '1', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('7', '2', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('8', '2', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('9', '2', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('10', '2', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('11', '2', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('12', '2', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('13', '3', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('14', '3', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('15', '3', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('16', '3', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('17', '3', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('18', '3', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('19', '4', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('20', '4', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('21', '4', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('22', '4', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('23', '4', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('24', '4', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('25', '5', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('26', '5', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('27', '5', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('28', '5', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('29', '5', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('30', '5', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('31', '6', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('32', '6', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('33', '6', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('34', '6', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('35', '6', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('36', '6', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('37', '7', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('38', '7', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('39', '7', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('40', '7', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('41', '7', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('42', '7', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('43', '8', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('44', '8', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('45', '8', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('46', '8', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('47', '8', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('48', '8', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('49', '9', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('50', '9', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('51', '9', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('52', '9', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('53', '9', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('54', '9', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('55', '10', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('56', '10', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('57', '10', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('58', '10', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('59', '10', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('60', '10', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('61', '11', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('62', '11', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('63', '11', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('64', '11', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('65', '11', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('66', '11', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('67', '12', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('68', '12', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('69', '12', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('70', '12', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('71', '12', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('72', '12', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('73', '13', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('74', '13', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('75', '13', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('76', '13', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('77', '13', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('78', '13', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('79', '14', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('80', '14', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('81', '14', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('82', '14', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('83', '14', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('84', '14', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('85', '15', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('86', '15', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('87', '15', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('88', '15', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('89', '15', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('90', '15', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('91', '16', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('92', '16', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('93', '16', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('94', '16', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('95', '16', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('96', '16', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('97', '17', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('98', '17', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('99', '17', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('100', '17', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('101', '17', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('102', '17', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('103', '18', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('104', '18', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('105', '18', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('106', '18', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('107', '18', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('108', '18', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('109', '19', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('110', '19', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('111', '19', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('112', '19', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('113', '19', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('114', '19', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('115', '20', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('116', '20', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('117', '20', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('118', '20', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('119', '20', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('120', '20', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('121', '21', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('122', '21', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('123', '21', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('124', '21', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('125', '21', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('126', '21', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('127', '22', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('128', '22', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('129', '22', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('130', '22', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('131', '22', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('132', '22', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('133', '23', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('134', '23', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('135', '23', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('136', '23', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('137', '23', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('138', '23', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('139', '24', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('140', '24', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('141', '24', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('142', '24', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('143', '24', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('144', '24', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('145', '25', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('146', '25', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('147', '25', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('148', '25', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('149', '25', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('150', '25', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('151', '26', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('152', '26', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('153', '26', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('154', '26', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('155', '26', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('156', '26', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('157', '27', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('158', '27', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('159', '27', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('160', '27', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('161', '27', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('162', '27', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('163', '28', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('164', '28', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('165', '28', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('166', '28', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('167', '28', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('168', '28', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('169', '29', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('170', '29', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('171', '29', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('172', '29', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('173', '29', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('174', '29', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('175', '30', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('176', '30', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('177', '30', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('178', '30', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('179', '30', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('180', '30', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('181', '31', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('182', '31', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('183', '31', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('184', '31', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('185', '31', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('186', '31', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('187', '32', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('188', '32', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('189', '32', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('190', '32', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('191', '32', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('192', '32', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('193', '33', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('194', '33', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('195', '33', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('196', '33', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('197', '33', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('198', '33', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('199', '34', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('200', '34', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('201', '34', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('202', '34', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('203', '34', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('204', '34', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('205', '35', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('206', '35', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('207', '35', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('208', '35', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('209', '35', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('210', '35', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('211', '36', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('212', '36', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('213', '36', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('214', '36', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('215', '36', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('216', '36', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('217', '37', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('218', '37', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('219', '37', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('220', '37', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('221', '37', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('222', '37', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('223', '38', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('224', '38', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('225', '38', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('226', '38', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('227', '38', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('228', '38', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('229', '39', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('230', '39', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('231', '39', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('232', '39', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('233', '39', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('234', '39', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('235', '40', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('236', '40', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('237', '40', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('238', '40', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('239', '40', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('240', '40', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('241', '41', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('242', '41', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('243', '41', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('244', '41', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('245', '41', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('246', '41', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('247', '42', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('248', '42', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('249', '42', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('250', '42', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('251', '42', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('252', '42', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('253', '43', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('254', '43', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('255', '43', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('256', '43', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('257', '43', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('258', '43', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('259', '44', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('260', '44', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('261', '44', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('262', '44', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('263', '44', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('264', '44', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('265', '45', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('266', '45', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('267', '45', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('268', '45', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('269', '45', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('270', '45', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('271', '46', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('272', '46', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('273', '46', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('274', '46', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('275', '46', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('276', '46', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('277', '47', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('278', '47', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('279', '47', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('280', '47', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('281', '47', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('282', '47', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('283', '48', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('284', '48', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('285', '48', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('286', '48', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('287', '48', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('288', '48', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('289', '49', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('290', '49', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('291', '49', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('292', '49', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('293', '49', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('294', '49', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('295', '50', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('296', '50', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('297', '50', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('298', '50', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('299', '50', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('300', '50', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('301', '51', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('302', '51', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('303', '51', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('304', '51', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('305', '51', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('306', '51', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('307', '52', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('308', '52', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('309', '52', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('310', '52', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('311', '52', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('312', '52', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('313', '53', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('314', '53', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('315', '53', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('316', '53', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('317', '53', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('318', '53', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('319', '54', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('320', '54', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('321', '54', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('322', '54', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('323', '54', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('324', '54', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('325', '55', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('326', '55', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('327', '55', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('328', '55', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('329', '55', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('330', '55', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('331', '56', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('332', '56', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('333', '56', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('334', '56', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('335', '56', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('336', '56', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('337', '57', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('338', '57', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('339', '57', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('340', '57', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('341', '57', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('342', '57', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('343', '58', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('344', '58', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('345', '58', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('346', '58', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('347', '58', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('348', '58', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('349', '59', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('350', '59', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('351', '59', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('352', '59', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('353', '59', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('354', '59', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('355', '60', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('356', '60', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('357', '60', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('358', '60', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('359', '60', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('360', '60', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('361', '61', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('362', '61', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('363', '61', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('364', '61', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('365', '61', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('366', '61', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('367', '62', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('368', '62', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('369', '62', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('370', '62', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('371', '62', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('372', '62', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('373', '63', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('374', '63', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('375', '63', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('376', '63', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('377', '63', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('378', '63', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('379', '64', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('380', '64', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('381', '64', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('382', '64', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('383', '64', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('384', '64', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('385', '65', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('386', '65', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('387', '65', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('388', '65', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('389', '65', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('390', '65', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('391', '66', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('392', '66', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('393', '66', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('394', '66', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('395', '66', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('396', '66', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('397', '67', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('398', '67', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('399', '67', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('400', '67', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('401', '67', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('402', '67', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('403', '68', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('404', '68', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('405', '68', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('406', '68', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('407', '68', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('408', '68', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('409', '69', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('410', '69', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('411', '69', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('412', '69', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('413', '69', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('414', '69', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('415', '70', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('416', '70', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('417', '70', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('418', '70', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('419', '70', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('420', '70', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('421', '71', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('422', '71', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('423', '71', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('424', '71', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('425', '71', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('426', '71', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('427', '72', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('428', '72', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('429', '72', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('430', '72', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('431', '72', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('432', '72', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('433', '73', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('434', '73', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('435', '73', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('436', '73', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('437', '73', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('438', '73', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('439', '74', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('440', '74', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('441', '74', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('442', '74', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('443', '74', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('444', '74', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('445', '75', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('446', '75', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('447', '75', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('448', '75', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('449', '75', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('450', '75', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('451', '76', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('452', '76', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('453', '76', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('454', '76', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('455', '76', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('456', '76', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('457', '77', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('458', '77', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('459', '77', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('460', '77', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('461', '77', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('462', '77', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('463', '78', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('464', '78', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('465', '78', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('466', '78', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('467', '78', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('468', '78', '6');
#
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('469', '79', '1');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('470', '79', '2');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('471', '79', '3');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('472', '79', '4');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('473', '79', '5');
# INSERT INTO test.rooms_objects (room_object_id, object_id, room_id) VALUES ('474', '79', '6');
