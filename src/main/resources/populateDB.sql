# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('1', 'First Book', 'First Author', '30000');
# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('2', 'Second Book', 'Second Author', '20000');
# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('3', 'Third Book', 'Third Author', '15000');
# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('4', 'Fourth Book', 'Fourth Author', '25000');
# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('5', 'Fifth Book', 'Fifth Author', '80000');
# INSERT INTO test.BOOK (ID, BOOK_TITLE, BOOK_AUTHOR, BOOK_PRICE) VALUES ('6', 'Sixth Book', 'Sixth Author', '50000');


SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema test
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema test
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `test` DEFAULT CHARACTER SET utf8 ;
USE `test` ;

-- -----------------------------------------------------
-- Table `test`.`regions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`regions` (
  `region_id` INT NOT NULL,
  `region` VARCHAR(255) NULL,
  PRIMARY KEY (`region_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`organizations`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`organizations` (
  `organizations_id` INT NOT NULL,
  `region_id` INT NOT NULL,
  `organization` VARCHAR(255) NULL,
  PRIMARY KEY (`organizations_id`),
  INDEX `fk__regions1_idx` (`region_id` ASC),
  CONSTRAINT `fk__regions1`
  FOREIGN KEY (`region_id`)
  REFERENCES `test`.`regions` (`region_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`subdivisions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`subdivisions` (
  `subdivision_id` INT NOT NULL,
  `organization_id` INT NOT NULL,
  `subdivision` VARCHAR(255) NULL,
  PRIMARY KEY (`subdivision_id`),
  INDEX `fk_subdivisions_1_idx` (`organization_id` ASC),
  CONSTRAINT `fk_subdivisions_1`
  FOREIGN KEY (`organization_id`)
  REFERENCES `test`.`organizations` (`organizations_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`objects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`objects` (
  `object_id` INT NOT NULL,
  `subdivision_id` INT NOT NULL,
  `object` VARCHAR(255) NULL,
  PRIMARY KEY (`object_id`),
  INDEX `fk_objects_subdivisions1_idx` (`subdivision_id` ASC),
  CONSTRAINT `fk_objects_subdivisions1`
  FOREIGN KEY (`subdivision_id`)
  REFERENCES `test`.`subdivisions` (`subdivision_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`rooms`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`rooms` (
  `room_id` INT NOT NULL,
  `room` VARCHAR(255) NULL,
  PRIMARY KEY (`room_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`rooms_objects`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`rooms_objects` (
  `room_object_id` INT NOT NULL,
  `object_id` INT NOT NULL,
  `room_id` INT NOT NULL,
  PRIMARY KEY (`room_object_id`),
  INDEX `fk_rooms_objects_rooms1_idx` (`room_id` ASC),
  INDEX `fk_rooms_objects_objects1_idx` (`object_id` ASC),
  CONSTRAINT `fk_rooms_objects_rooms1`
  FOREIGN KEY (`room_id`)
  REFERENCES `test`.`rooms` (`room_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rooms_objects_objects1`
  FOREIGN KEY (`object_id`)
  REFERENCES `test`.`objects` (`object_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`photos`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`photos` (
  `photo_id` INT NOT NULL,
  `photo` VARCHAR(255) NULL,
  PRIMARY KEY (`photo_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`miniatures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`miniatures` (
  `miniature_id` INT NOT NULL,
  `miniature` VARCHAR(255) NULL,
  PRIMARY KEY (`miniature_id`))
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`photos_miniatures`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`photos_miniatures` (
  `photo_miniature_id` INT NOT NULL,
  `data_time` DATETIME NULL,
  `photo_id` INT NOT NULL,
  `miniature_id` INT NOT NULL,
  `room_object_id` INT NOT NULL,
  `flag` INT NULL,
  PRIMARY KEY (`photo_miniature_id`),
  INDEX `fk_photos_miniatures_photos1_idx` (`photo_id` ASC),
  INDEX `fk_photos_miniatures_miniatures1_idx` (`miniature_id` ASC),
  INDEX `fk_photos_miniatures_rooms_objects1_idx` (`room_object_id` ASC),
  CONSTRAINT `fk_photos_miniatures_photos1`
  FOREIGN KEY (`photo_id`)
  REFERENCES `test`.`photos` (`photo_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_photos_miniatures_miniatures1`
  FOREIGN KEY (`miniature_id`)
  REFERENCES `test`.`miniatures` (`miniature_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_photos_miniatures_rooms_objects1`
  FOREIGN KEY (`room_object_id`)
  REFERENCES `test`.`rooms_objects` (`room_object_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `test`.`missions`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `test`.`missions` (
  `mission_id` INT NOT NULL,
  `data_time` DATETIME NULL,
  `room_object_id` INT NOT NULL,
  `photo_miniature_id` INT NOT NULL,
  `description` VARCHAR(255) NULL,
  `term` DATETIME NULL,
  `criteria` VARCHAR(255) NULL,
  `value` INT NULL,
  PRIMARY KEY (`mission_id`),
  INDEX `fk_missions_rooms_objects1_idx` (`room_object_id` ASC),
  INDEX `fk_missions_photos_miniatures1_idx` (`photo_miniature_id` ASC),
  CONSTRAINT `fk_missions_rooms_objects1`
  FOREIGN KEY (`room_object_id`)
  REFERENCES `test`.`rooms_objects` (`room_object_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_missions_photos_miniatures1`
  FOREIGN KEY (`photo_miniature_id`)
  REFERENCES `test`.`photos_miniatures` (`photo_miniature_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
  ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
