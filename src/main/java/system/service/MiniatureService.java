package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.MiniatureDao;
import system.model.Miniature;

import java.util.List;

@Service
public class MiniatureService {

    private MiniatureDao miniatureDao;


    @Transactional
    public void addMiniature(Miniature miniature) {
        this.miniatureDao.addMiniature(miniature);

    }


    @Transactional
    public void updateMiniature(Miniature miniature) {
        this.miniatureDao.updateMiniature(miniature);
    }


    @Transactional
    public void removeMiniature(int id) {
        this.miniatureDao.removeMiniature(id);
    }


    @Transactional
    public Miniature getMiniatureById(int id) {
        return this.miniatureDao.getMiniatureById(id);
    }


    @Transactional
    public List<Miniature> listMiniatures() {
        return this.miniatureDao.listMiniatures();
    }

    public void setMiniatureDao(MiniatureDao miniatureDao) {
        this.miniatureDao = miniatureDao;
    }

    public MiniatureDao getMiniatureDao() {
        return miniatureDao;
    }
}
