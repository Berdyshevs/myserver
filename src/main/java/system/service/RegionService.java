package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.RegionDao;
import system.model.Book;
import system.model.Region;

import java.util.List;

@Service
public class RegionService {

    private RegionDao regionDao;


    @Transactional
    public void addRegion(Region region) {
        this.regionDao.addRegion(region);

    }


    @Transactional
    public void updateRegion(Region region) {
        this.regionDao.updateRegion(region);
    }


    @Transactional
    public void removeRegion(int id) {
        this.regionDao.removeRegion(id);
    }


    @Transactional
    public Region getRegionById(int id) {
        return this.regionDao.getRegionById(id);
    }


    @Transactional
    public List<Region> listRegions() {
        return this.regionDao.listRegions();
    }

    public void setRegionDao(RegionDao regionDao) {
        this.regionDao = regionDao;
    }

    public RegionDao getRegionDao() {
        return regionDao;
    }
}
