package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.PhotoDao;
import system.dao.RegionDao;
import system.model.Photo;
import system.model.Region;

import java.util.List;

@Service
public class PhotoService {

    private PhotoDao photoDao;


    @Transactional
    public void addPhoto(Photo photo) {
        this.photoDao.addPhoto(photo);

    }


    @Transactional
    public void updatePhoto(Photo photo) {
        this.photoDao.updatePhoto(photo);
    }


    @Transactional
    public void removePhoto(int id) {
        this.photoDao.removePhoto(id);
    }


    @Transactional
    public Photo getPhotoById(int id) {
        return this.photoDao.getPhotoById(id);
    }


    @Transactional
    public List<Photo> listPhotos() {
        return this.photoDao.listPhotos();
    }

    public void setPhotoDao(PhotoDao photoDao) {
        this.photoDao = photoDao;
    }

    public PhotoDao getPhotoDao() {
        return photoDao;
    }
}
