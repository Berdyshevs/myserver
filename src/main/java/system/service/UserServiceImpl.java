package system.service;

import org.springframework.transaction.annotation.Transactional;
import system.dao.RoleDao;
import system.dao.UserDao;
//import system.dao.UsersDao;
import system.model.Role;
import system.model.User;
import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;



@Service
public class UserServiceImpl implements UserService {

//    private UsersDao usersDao;

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleDao roleDao;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Override
    public void save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        Set<Role> roles = new HashSet<>();
        roles.add(roleDao.getOne(1L));
        user.setRoles(roles);
        userDao.save(user);
    }

    @Override
    public User findByUsername(String username) {
        System.out.println("< " + userDao.findByUsername(username));
        return userDao.findByUsername(username);
    }



//        @Transactional
//    public List<User> listUsers() {
//        return this.usersDao.listUsers();
//    }
}
