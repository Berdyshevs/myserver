package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.MissionDao;

import system.model.Mission;


import java.util.List;

@Service
public class MissionService {

    private MissionDao missionDao;


    @Transactional
    public void addMission(Mission mission) {
        this.missionDao.addMission(mission);

    }


    @Transactional
    public void updateMission(Mission mission) {
        this.missionDao.updateMission(mission);
    }


    @Transactional
    public void removeMission(int id) {
        this.missionDao.removeMission(id);
    }


    @Transactional
    public Mission getMissionById(int id) {
        return this.missionDao.getMissionById(id);
    }


    @Transactional
    public List<Mission> listMissions() {
        return this.missionDao.listMissions();
    }

    public void setMissionDao(MissionDao missionDao) {
        this.missionDao = missionDao;
    }

    public MissionDao getMissionDao() {
        return missionDao;
    }
}
