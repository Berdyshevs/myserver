package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.PhotoMiniatureDao;
import system.model.PhotoMiniature;

import java.util.List;

@Service
public class PhotoMiniatureService {

    private PhotoMiniatureDao photoMiniatureDao;


    @Transactional
    public void addPhotoMiniature(PhotoMiniature photoMiniature) {
        this.photoMiniatureDao.addPhotoMiniature(photoMiniature);

    }


    @Transactional
    public void updatePhotoMiniature(PhotoMiniature photoMiniature) {
        this.photoMiniatureDao.updatePhotoMiniature(photoMiniature);
    }


    @Transactional
    public void removePhotoMiniature(int id) {
        this.photoMiniatureDao.removePhotoMiniature(id);
    }


    @Transactional
    public PhotoMiniature getPhotoMiniatureById(int id) {
        return this.photoMiniatureDao.getPhotoMiniatureById(id);
    }


    @Transactional
    public List<PhotoMiniature> listPhotoMiniatures() {
        return this.photoMiniatureDao.listPhotoMiniatures();
    }

    public void setPhotoMiniatureDao(PhotoMiniatureDao photoMiniatureDao) {
        this.photoMiniatureDao = photoMiniatureDao;
    }

    public PhotoMiniatureDao getPhotoMiniatureDao() {
        return photoMiniatureDao;
    }
}
