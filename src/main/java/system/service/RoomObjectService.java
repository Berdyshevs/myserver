package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.RoomObjectDao;
import system.model.RoomObject;
import java.util.List;

@Service
public class RoomObjectService {

    private RoomObjectDao roomObjectDao;


    @Transactional
    public void addRoomObject(RoomObject roomObject) {
        this.roomObjectDao.addRoomObject(roomObject);

    }


    @Transactional
    public void updateRoomObject(RoomObject roomObject) {
        this.roomObjectDao.updateRoomObject(roomObject);
    }


    @Transactional
    public void removeRoomObject(int id) {
        this.roomObjectDao.removeRoomObject(id);
    }


    @Transactional
    public RoomObject getRoomObjectById(int id) {
        return this.roomObjectDao.getRoomObjectById(id);
    }


    @Transactional
    public List<RoomObject> listRoomObjects() {
        return this.roomObjectDao.listRoomObjects();
    }

    public void setRoomObjectDao(RoomObjectDao roomObjectDao) {
        this.roomObjectDao = roomObjectDao;
    }

    public RoomObjectDao getRoomObjectDao() {
        return roomObjectDao;
    }
}
