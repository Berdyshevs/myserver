package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.OrganizationDao;
import system.model.Organization;


import java.util.List;

@Service
public class OrganizationService {

    private OrganizationDao organizationDao;


    @Transactional
    public void addOrganization(Organization organization) {
        this.organizationDao.addOrganization(organization);

    }


    @Transactional
    public void updateOrganization(Organization organization) {
        this.organizationDao.updateOrganization(organization);
    }


    @Transactional
    public void removeOrganization(int id) {
        this.organizationDao.removeOrganization(id);
    }


    @Transactional
    public Organization getOrganizationById(int id) {
        return this.organizationDao.getOrganizationById(id);
    }


    @Transactional
    public List<Organization> listOrganizations() {
        return this.organizationDao.listOrganizations();
    }

    public void setOrganizationDao(OrganizationDao organizationDao) {
        this.organizationDao = organizationDao;
    }

    public OrganizationDao getOrganizationDao() {
        return organizationDao;
    }
}
