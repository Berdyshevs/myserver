package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.SubdivisionDao;
import system.model.Subdivision;

import java.util.List;

@Service
public class SubdivisionService {

    private SubdivisionDao subdivisionDao;


    @Transactional
    public void addSubdivision(Subdivision subdivision) {
        this.subdivisionDao.addSubdivision(subdivision);

    }


    @Transactional
    public void updateSubdivision(Subdivision subdivision) {
        this.subdivisionDao.updateSubdivision(subdivision);
    }


    @Transactional
    public void removeSubdivision(int id) {
        this.subdivisionDao.removeSubdivision(id);
    }


    @Transactional
    public Subdivision getSubdivisionById(int id) {
        return this.subdivisionDao.getSubdivisionById(id);
    }


    @Transactional
    public List<Subdivision> listSubdivisions() {
        return this.subdivisionDao.listSubdivisions();
    }

    public void setSubdivisionDao(SubdivisionDao subdivisionDao) {
        this.subdivisionDao = subdivisionDao;
    }

    public SubdivisionDao getSubdivisionDao() {
        return subdivisionDao;
    }
}
