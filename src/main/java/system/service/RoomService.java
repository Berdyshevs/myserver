package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.RoomDao;
import system.model.Room;

import java.util.List;

@Service
public class RoomService {

    private RoomDao roomDao;


    @Transactional
    public void addRoom(Room room) {
        this.roomDao.addRoom(room);

    }


    @Transactional
    public void updateRoom(Room room) {
        this.roomDao.updateRoom(room);
    }


    @Transactional
    public void removeRoom(int id) {
        this.roomDao.removeRoom(id);
    }


    @Transactional
    public Room getRoomById(int id) {
        return this.roomDao.getRoomById(id);
    }


    @Transactional
    public List<Room> listRooms() {
        return this.roomDao.listRooms();
    }

    public void setRoomDao(RoomDao RoomDao) {
        this.roomDao = RoomDao;
    }

    public RoomDao getRoomDao() {
        return roomDao;
    }
}
