package system.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import system.dao.ObjectDao;


import java.util.List;

@Service
public class ObjectService {

    private ObjectDao objectDao;


    @Transactional
    public void addObject(Object object) {
        this.objectDao.addObject(object);

    }


    @Transactional
    public void updateObject(Object object) {
        this.objectDao.updateObject(object);
    }


    @Transactional
    public void removeObject(int id) {
        this.objectDao.removeObject(id);
    }


    @Transactional
    public Object getObjectById(int id) {
        return this.objectDao.getObjectById(id);
    }


    @Transactional
    public List<Object> listObjects() {
        return this.objectDao.listObjects();
    }

    public void setObjectDao(ObjectDao objectDao) {
        this.objectDao = objectDao;
    }

    public ObjectDao getObjectDao() {
        return objectDao;
    }
}
