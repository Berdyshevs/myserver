package system.service;

import system.model.User;


public interface UserService {

    void save(User user);

    User findByUsername(String username);


}
