package system.model;


import javax.persistence.*;

@Entity
@Table(name = "miniatures")
public class Miniature {
    @Id
    @Column(name = "miniature_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "miniature")
    private String miniature;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMiniature() {
        return miniature;
    }

    public void setMiniature(String miniature) {
        this.miniature = miniature;
    }
}
