package system.model;


import javax.persistence.*;

@Entity
@Table(name = "missions")
public class Mission {
    @Id
    @Column(name = "mission_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "data_time")
    private String data_time;


    @Column(name = "room_object_id")
    private int room_object_id;

    @Column(name = "photo_miniature_id")
    private int photo_miniature_id;

    @Column(name = "description")
    private int description;

    @Column(name = "term")
    private String term;

    @Column(name = "criteria")
    private String criteria;

    @Column(name = "value")
    private String value;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData_time() {
        return data_time;
    }

    public void setData_time(String data_time) {
        this.data_time = data_time;
    }

    public int getRoom_object_id() {
        return room_object_id;
    }

    public void setRoom_object_id(int room_object_id) {
        this.room_object_id = room_object_id;
    }

    public int getPhoto_miniature_id() {
        return photo_miniature_id;
    }

    public void setPhoto_miniature_id(int photo_miniature_id) {
        this.photo_miniature_id = photo_miniature_id;
    }

    public int getDescription() {
        return description;
    }

    public void setDescription(int description) {
        this.description = description;
    }

    public String getTerm() {
        return term;
    }

    public void setTerm(String term) {
        this.term = term;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
