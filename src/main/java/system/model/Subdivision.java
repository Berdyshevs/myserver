package system.model;


import javax.persistence.*;

@Entity
@Table(name = "subdivisions")
public class Subdivision {
    @Id
    @Column(name = "subdivision_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "organization_id")
    private int organization_id;

    @Column(name = "subdivision")
    private String subdivision;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getOrganization_id() {
        return organization_id;
    }

    public void setOrganization_id(int organization_id) {
        this.organization_id = organization_id;
    }

    public String getSubdivision() {
        return subdivision;
    }

    public void setSubdivision(String subdivision) {
        this.subdivision = subdivision;
    }
}
