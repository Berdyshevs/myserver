package system.model;


import javax.persistence.*;

@Entity
@Table(name = "photos_miniatures")
public class PhotoMiniature {
    @Id
    @Column(name = "photo_miniature_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "data_time")
    private String data_time;

    @Column(name = "photo_id")
    private int photo_id;

    @Column(name = "miniature_id")
    private int miniature_id;

    @Column(name = "room_object_id")
    private int room_object_id;

    @Column(name = "flag")
    private int flag;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getData_time() {
        return data_time;
    }

    public void setData_time(String data_time) {
        this.data_time = data_time;
    }

    public int getPhoto_id() {
        return photo_id;
    }

    public void setPhoto_id(int photo_id) {
        this.photo_id = photo_id;
    }

    public int getMiniature_id() {
        return miniature_id;
    }

    public void setMiniature_id(int miniature_id) {
        this.miniature_id = miniature_id;
    }

    public int getRoom_object_id() {
        return room_object_id;
    }

    public void setRoom_object_id(int room_object_id) {
        this.room_object_id = room_object_id;
    }

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }
}
