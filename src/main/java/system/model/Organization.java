package system.model;

import javax.persistence.*;


@Entity
@Table(name = "organizations")
public class Organization {

    @Id
    @Column(name = "organizations_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "region_id")
    private int region_id;


    @Column(name = "organization")
    private String organization;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getRegion_id() {
        return region_id;
    }

    public void setRegion_id(int region_id) {
        this.region_id = region_id;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }
}
