package system.validator;

import system.model.User;
import system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;


@Component
public class UserValidator implements Validator {

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> aClass) {
        return User.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        User user = (User) o;


        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "uploadForm.selectFile", "Необходимо заполнить поле.");
        if (user.getUsername().length() < 3 || user.getUsername().length() > 32) {
            errors.rejectValue("username", "uploadForm.selectFile", "Имя должно содержать от 3 до 32 символов.");
        }

        if (userService.findByUsername(user.getUsername()) != null) {
            errors.rejectValue("username", "uploadForm.selectFile", "Такое имя уже существует.");
        }

        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "uploadForm.selectFile", "Необходимо заполнить поле.");
        if (user.getPassword().length() < 5 || user.getPassword().length() > 32) {
            errors.rejectValue("password", "uploadForm.selectFile", "Пароль должен содержать более 5 символов.");
        }

        if (!user.getConfirmPassword().equals(user.getPassword())) {
            errors.rejectValue("confirmPassword", "uploadForm.selectFile", "Неверный пароль.");
        }



    }
}
