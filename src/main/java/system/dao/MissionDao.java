package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Mission;

import java.util.List;

@Repository
public class MissionDao {

    private static final Logger logger = LoggerFactory.getLogger(MissionDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addMission(Mission mission) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(mission);
        logger.info("Mission successfully saved. Mission details: " + mission);

    }

    public void updateMission(Mission mission) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(mission);
        logger.info("Mission successfully update. Mission details: " + mission);

    }

    public void removeMission(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Mission mission = (Mission) session.load(Mission.class, new Integer(id));

        if (mission != null) {
            session.delete(mission);
        }
        logger.info("Mission successfully removed. Mission details: " + mission);

    }

    public Mission getMissionById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Mission mission = (Mission) session.load(Mission.class, new Integer(id));
        logger.info("Mission successfully loaded. Mission details: " + mission);
        return mission;
    }

    @SuppressWarnings("unchecked")
    public List<Mission> listMissions() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Mission> missionList = session.createQuery("from Mission").list();

        for (Mission mission : missionList) {

            logger.info("Mission list: " + mission);


        }

        return missionList;
    }


}
