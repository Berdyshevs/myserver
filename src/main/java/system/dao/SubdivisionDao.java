package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Subdivision;

import java.util.List;

@Repository
public class SubdivisionDao {

    private static final Logger logger = LoggerFactory.getLogger(SubdivisionDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addSubdivision(Subdivision subdivision) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(subdivision);
        logger.info("Subdivision successfully saved. Subdivision details: " + subdivision);

    }

    public void updateSubdivision(Subdivision subdivision) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(subdivision);
        logger.info("Subdivision successfully update. Subdivision details: " + subdivision);

    }

    public void removeSubdivision(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Subdivision subdivision = (Subdivision) session.load(Subdivision.class, new Integer(id));

        if (subdivision != null) {
            session.delete(subdivision);
        }
        logger.info("Subdivision successfully removed. Subdivision details: " + subdivision);

    }

    public Subdivision getSubdivisionById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Subdivision subdivision = (Subdivision) session.load(Subdivision.class, new Integer(id));
        logger.info("Subdivision successfully loaded. Subdivision details: " + subdivision);
        return subdivision;
    }

    @SuppressWarnings("unchecked")
    public List<Subdivision> listSubdivisions() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Subdivision> subdivisionList = session.createQuery("from Subdivision").list();

        for (Subdivision subdivision : subdivisionList) {

            logger.info("Subdivision list: " + subdivision);


        }

        return subdivisionList;
    }


}
