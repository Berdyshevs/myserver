package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;


import java.util.List;

@Repository
public class ObjectDao {

    private static final Logger logger = LoggerFactory.getLogger(ObjectDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addObject(Object object) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(object);
        logger.info("Object successfully saved. Object details: " + object);

    }

    public void updateObject(Object object) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(object);
        logger.info("Object successfully update. Object details: " + object);

    }

    public void removeObject(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Object object = (Object) session.load(Object.class, new Integer(id));

        if (object != null) {
            session.delete(object);
        }
        logger.info("Object successfully removed. Object details: " + object);

    }

    public Object getObjectById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Object object = (Object) session.load(Object.class, new Integer(id));
        logger.info("Object successfully loaded. Object details: " + object);
        return object;
    }

    @SuppressWarnings("unchecked")
    public List<Object> listObjects() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Object> objectList = session.createQuery("from Object").list(); // мб не будет работать

        for (Object object : objectList) {

            logger.info("Object list: " + object);


        }

        return objectList;
    }


}
