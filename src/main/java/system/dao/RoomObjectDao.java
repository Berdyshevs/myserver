package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.RoomObject;

import java.util.List;

@Repository
public class RoomObjectDao {

    private static final Logger logger = LoggerFactory.getLogger(RoomObjectDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addRoomObject(RoomObject roomObject) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(roomObject);
        logger.info("RoomObject successfully saved. RoomObject details: " + roomObject);

    }

    public void updateRoomObject(RoomObject roomObject) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(roomObject);
        logger.info("RoomObject successfully update. RoomObject details: " + roomObject);

    }

    public void removeRoomObject(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        RoomObject roomObject = (RoomObject) session.load(RoomObject.class, new Integer(id));

        if (roomObject != null) {
            session.delete(roomObject);
        }
        logger.info("RoomObject successfully removed. RoomObject details: " + roomObject);

    }

    public RoomObject getRoomObjectById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        RoomObject roomObject = (RoomObject) session.load(RoomObject.class, new Integer(id));
        logger.info("RoomObject successfully loaded. RoomObject details: " + roomObject);
        return roomObject;
    }

    @SuppressWarnings("unchecked")
    public List<RoomObject> listRoomObjects() {
        Session session = this.sessionFactory.getCurrentSession();
        List<RoomObject> roomObjectList = session.createQuery("from RoomObject").list();

        for (RoomObject roomObject : roomObjectList) {

            logger.info("RoomObject list: " + roomObject);


        }

        return roomObjectList;
    }


}
