package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Miniature;

import java.util.List;

@Repository
public class MiniatureDao {

    private static final Logger logger = LoggerFactory.getLogger(MiniatureDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addMiniature(Miniature miniature) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(miniature);
        logger.info("Miniature successfully saved. Miniature details: " + miniature);

    }

    public void updateMiniature(Miniature miniature) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(miniature);
        logger.info("Miniature successfully update. Miniature details: " + miniature);

    }

    public void removeMiniature(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Miniature miniature = (Miniature) session.load(Miniature.class, new Integer(id));

        if (miniature != null) {
            session.delete(miniature);
        }
        logger.info("Miniature successfully removed. Miniature details: " + miniature);

    }

    public Miniature getMiniatureById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Miniature miniature = (Miniature) session.load(Miniature.class, new Integer(id));
        logger.info("Miniature successfully loaded. Miniature details: " + miniature);
        return miniature;
    }

    @SuppressWarnings("unchecked")
    public List<Miniature> listMiniatures() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Miniature> miniatureList = session.createQuery("from Miniature").list();

        for (Miniature miniature : miniatureList) {

            logger.info("Miniature list: " + miniature);


        }

        return miniatureList;
    }


}
