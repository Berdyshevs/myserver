package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import org.slf4j.Logger;
import system.model.Region;

import java.util.List;

@Repository
public class RegionDao {

    private static final Logger logger = LoggerFactory.getLogger(RegionDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addRegion(Region region) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(region);
        logger.info("Region successfully saved. Region details: " + region);

    }

    public void updateRegion(Region region) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(region);
        logger.info("Region successfully update. Region details: " + region);

    }

    public void removeRegion(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Region region = (Region) session.load(Region.class, new Integer(id));

        if (region != null) {
            session.delete(region);
        }
        logger.info("Region successfully removed. Region details: " + region);

    }

    public Region getRegionById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Region region = (Region) session.load(Region.class, new Integer(id));
        logger.info("Region successfully loaded. Region details: " + region);
        return region;
    }

    @SuppressWarnings("unchecked")
    public List<Region> listRegions() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Region> regionList = session.createQuery("from Region").list();

        for (Region region : regionList) {

            logger.info("Region list: " + region);


        }

        return regionList;
    }


}
