package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Organization;


import java.util.List;

@Repository
public class OrganizationDao {

    private static final Logger logger = LoggerFactory.getLogger(OrganizationDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addOrganization(Organization organization) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(organization);
        logger.info("Organization successfully saved. Organization details: " + organization);

    }

    public void updateOrganization(Organization organization) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(organization);
        logger.info("Organization successfully update. Organization details: " + organization);

    }

    public void removeOrganization(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Organization organization = (Organization) session.load(Organization.class, new Integer(id));

        if (organization != null) {
            session.delete(organization);
        }
        logger.info("Organization successfully removed. Organization details: " + organization);

    }

    public Organization getOrganizationById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Organization organization = (Organization) session.load(Organization.class, new Integer(id));
        logger.info("Organization successfully loaded. Organization details: " + organization);
        return organization;
    }

    @SuppressWarnings("unchecked")
    public List<Organization> listOrganizations() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Organization> organizationList = session.createQuery("from Organization").list();

        for (Organization organization : organizationList) {

            logger.info("Organization list: " + organization);


        }

        return organizationList;
    }


}
