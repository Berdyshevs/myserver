package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.PhotoMiniature;

import java.util.List;

@Repository
public class PhotoMiniatureDao {

    private static final Logger logger = LoggerFactory.getLogger(PhotoMiniatureDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addPhotoMiniature(PhotoMiniature photoMiniature) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(photoMiniature);
        logger.info("PhotoMiniature successfully saved. PhotoMiniature details: " + photoMiniature);

    }

    public void updatePhotoMiniature(PhotoMiniature photoMiniature) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(photoMiniature);
        logger.info("PhotoMiniature successfully update. PhotoMiniature details: " + photoMiniature);

    }

    public void removePhotoMiniature(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        PhotoMiniature photoMiniature = (PhotoMiniature) session.load(PhotoMiniature.class, new Integer(id));

        if (photoMiniature != null) {
            session.delete(photoMiniature);
        }
        logger.info("PhotoMiniature successfully removed. PhotoMiniature details: " + photoMiniature);

    }

    public PhotoMiniature getPhotoMiniatureById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        PhotoMiniature photoMiniature = (PhotoMiniature) session.load(PhotoMiniature.class, new Integer(id));
        logger.info("PhotoMiniature successfully loaded. PhotoMiniature details: " + photoMiniature);
        return photoMiniature;
    }

    @SuppressWarnings("unchecked")
    public List<PhotoMiniature> listPhotoMiniatures() {
        Session session = this.sessionFactory.getCurrentSession();
        List<PhotoMiniature> photoMiniatureList = session.createQuery("from PhotoMiniature").list();

        for (PhotoMiniature photoMiniature : photoMiniatureList) {

            logger.info("PhotoMiniature list: " + photoMiniature);


        }

        return photoMiniatureList;
    }


}
