package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Photo;


import java.util.List;

@Repository
public class PhotoDao {

    private static final Logger logger = LoggerFactory.getLogger(PhotoDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addPhoto(Photo photo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(photo);
        logger.info("Photo successfully saved. Photo details: " + photo);

    }

    public void updatePhoto(Photo photo) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(photo);
        logger.info("Photo successfully update. Photo details: " + photo);

    }

    public void removePhoto(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Photo photo = (Photo) session.load(Photo.class, new Integer(id));

        if (photo != null) {
            session.delete(photo);
        }
        logger.info("Photo successfully removed. Photo details: " + photo);

    }

    public Photo getPhotoById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Photo photo = (Photo) session.load(Photo.class, new Integer(id));
        logger.info("Photo successfully loaded. Photo details: " + photo);
        return photo;
    }

    @SuppressWarnings("unchecked")
    public List<Photo> listPhotos() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Photo> photoList = session.createQuery("from Photo").list();

        for (Photo photo : photoList) {

            logger.info("Photo list: " + photo);


        }

        return photoList;
    }


}
