package system.dao;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;
import system.model.Room;

import java.util.List;

@Repository
public class RoomDao {

    private static final Logger logger = LoggerFactory.getLogger(RoomDao.class);

    private SessionFactory sessionFactory;

    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;

    }

    public void addRoom(Room room) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(room);
        logger.info("Room successfully saved. Room details: " + room);

    }

    public void updateRoom(Room room) {
        Session session = this.sessionFactory.getCurrentSession();
        session.update(room);
        logger.info("Room successfully update. Room details: " + room);

    }

    public void removeRoom(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Room room = (Room) session.load(Room.class, new Integer(id));

        if (room != null) {
            session.delete(room);
        }
        logger.info("Room successfully removed. Room details: " + room);

    }

    public Room getRoomById(int id) {
        Session session = this.sessionFactory.getCurrentSession();
        Room room = (Room) session.load(Room.class, new Integer(id));
        logger.info("Room successfully loaded. Room details: " + room);
        return room;
    }

    @SuppressWarnings("unchecked")
    public List<Room> listRooms() {
        Session session = this.sessionFactory.getCurrentSession();
        List<Room> roomList = session.createQuery("from Room").list();

        for (Room room : roomList) {

            logger.info("Room list: " + room);


        }

        return roomList;
    }


}
