package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Mission;
import system.service.MissionService;


import java.util.List;

@Controller
public class MissionController {
    private MissionService missionService;

    @Autowired(required = true)
    @Qualifier(value = "missionService")
    public void setMissionService(MissionService missionService) {
        this.missionService = missionService;
    }

//    @RequestMapping(value = "missions", method = RequestMethod.GET)
//    public String listMissions(Model model){
//        model.addAttribute("mission", new Mission());
//        model.addAttribute("listMissions", this.missionService.listMissions());
//
//        return "missions";
//    }
//
//    @RequestMapping(value = "/missions/add", method = RequestMethod.POST)
//    public String addMission(@ModelAttribute("mission") Mission mission){
//        if(mission.getId() == 0){
//            this.missionService.addMission(mission);
//        }else {
//            this.missionService.updateMission(mission);
//        }
//
//        return "redirect:/missions";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeMission(@PathVariable("id") int id){
//        this.missionService.removeMission(id);
//
//        return "redirect:/missions";
//    }
//
//    @RequestMapping("edit-mission/{id}")
//    public String editMission(@PathVariable("id") int id, Model model){
//        model.addAttribute("mission", this.missionService.getMissionById(id));
//        model.addAttribute("listMissions", this.missionService.listMissions());
//
//        return "missions";
//    }
//
//    @RequestMapping("missiondata/{id}")
//    public String missionData(@PathVariable("id") int id, Model model){
//        model.addAttribute("mission", this.missionService.getMissionById(id));
//
//        return "missiondata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/missions", method = RequestMethod.GET)
    public @ResponseBody

    List<Mission> listMissions() {

        return missionService.listMissions();

    }

}
