package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Object;
import system.service.ObjectService;

import java.util.List;

@Controller
public class ObjectController {
    private ObjectService objectService;

    @Autowired(required = true)
    @Qualifier(value = "objectService")
    public void setObjectService(ObjectService objectService) {
        this.objectService = objectService;
    }

//    @RequestMapping(value = "objects", method = RequestMethod.GET)
//    public String listObjects(Model model){
//        model.addAttribute("object", new Object());
//        model.addAttribute("listObjects", this.objectService.listObjects());
//
//        return "objects";
//    }
//
//    @RequestMapping(value = "/objects/add", method = RequestMethod.POST)
//    public String addObject(@ModelAttribute("object") Object object){
//        if(object.getId() == 0){
//            this.objectService.addObject(object);
//        }else {
//            this.objectService.updateObject(object);
//        }
//
//        return "redirect:/objects";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeObject(@PathVariable("id") int id){
//        this.objectService.removeObject(id);
//
//        return "redirect:/objects";
//    }
//
//    @RequestMapping("edit-object/{id}")
//    public String editObject(@PathVariable("id") int id, Model model){
//        model.addAttribute("object", this.objectService.getObjectById(id));
//        model.addAttribute("listObjects", this.objectService.listObjects());
//
//        return "objects";
//    }
//
//    @RequestMapping("objectdata/{id}")
//    public String objectData(@PathVariable("id") int id, Model model){
//        model.addAttribute("object", this.objectService.getObjectById(id));
//
//        return "objectdata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/objects", method = RequestMethod.GET)
    public @ResponseBody
    List<java.lang.Object> listObjects() {

        return objectService.listObjects();

    }

}
