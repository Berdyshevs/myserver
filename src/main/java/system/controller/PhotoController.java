package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import system.model.Photo;
import system.service.PhotoService;


import java.util.List;

@Controller
public class PhotoController {
    private PhotoService photoService;

    @Autowired(required = true)
    @Qualifier(value = "photoService")
    public void setPhotoService(PhotoService photoService) {
        this.photoService = photoService;
    }

//    @RequestMapping(value = "photos", method = RequestMethod.GET)
//    public String listPhotos(Model model){
//        model.addAttribute("photo", new Photo());
//        model.addAttribute("listPhotos", this.photoService.listPhotos());
//
//        return "photos";
//    }
//
//    @RequestMapping(value = "/photos/add", method = RequestMethod.POST)
//    public String addPhoto(@ModelAttribute("photo") Photo photo){
//        if(photo.getId() == 0){
//            this.photoService.addPhoto(photo);
//        }else {
//            this.photoService.updatePhoto(photo);
//        }
//
//        return "redirect:/photos";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removePhoto(@PathVariable("id") int id){
//        this.photoService.removePhoto(id);
//
//        return "redirect:/photos";
//    }
//
//    @RequestMapping("edit-photo/{id}")
//    public String editPhoto(@PathVariable("id") int id, Model model){
//        model.addAttribute("photo", this.photoService.getPhotoById(id));
//        model.addAttribute("listPhotos", this.photoService.listPhotos());
//
//        return "photos";
//    }
//
//    @RequestMapping("photodata/{id}")
//    public String photoData(@PathVariable("id") int id, Model model){
//        model.addAttribute("photo", this.photoService.getPhotoById(id));
//
//        return "photodata";
//    }



    // формат для JSON
    @RequestMapping(value = "/json/photos", method = RequestMethod.GET)
    public @ResponseBody

    List<Photo> listPhotos() {

        return photoService.listPhotos();

    }

}
