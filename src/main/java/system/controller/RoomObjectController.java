package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import system.model.RoomObject;
import system.service.RoomObjectService;

import java.util.List;

@Controller
public class RoomObjectController {
    private RoomObjectService roomObjectService;

    @Autowired(required = true)
    @Qualifier(value = "roomObjectService")
    public void setRoomObjectService(RoomObjectService roomObjectService) {
        this.roomObjectService = roomObjectService;
    }

//    @RequestMapping(value = "roomObjects", method = RequestMethod.GET)
//    public String listRoomObjects(Model model){
//        model.addAttribute("roomObject", new RoomObject());
//        model.addAttribute("listRoomObjects", this.roomObjectService.listRoomObjects());
//
//        return "roomObjects";
//    }
//
//    @RequestMapping(value = "/roomObjects/add", method = RequestMethod.POST)
//    public String addRoomObject(@ModelAttribute("roomObject") RoomObject roomObject){
//        if(roomObject.getId() == 0){
//            this.roomObjectService.addRoomObject(roomObject);
//        }else {
//            this.roomObjectService.updateRoomObject(roomObject);
//        }
//
//        return "redirect:/roomObjects";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeRoomObject(@PathVariable("id") int id){
//        this.roomObjectService.removeRoomObject(id);
//
//        return "redirect:/roomObjects";
//    }
//
//    @RequestMapping("edit-roomObject/{id}")
//    public String editRoomObject(@PathVariable("id") int id, Model model){
//        model.addAttribute("roomObject", this.roomObjectService.getRoomObjectById(id));
//        model.addAttribute("listRoomObjects", this.roomObjectService.listRoomObjects());
//
//        return "roomObjects";
//    }
//
//    @RequestMapping("roomObjectdata/{id}")
//    public String roomObjectData(@PathVariable("id") int id, Model model){
//        model.addAttribute("roomObject", this.roomObjectService.getRoomObjectById(id));
//
//        return "roomObjectdata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/roomobjects", method = RequestMethod.GET)
    public @ResponseBody

    List<RoomObject> listRoomObjects() {

        return roomObjectService.listRoomObjects();

    }

}
