package system.controller;

import com.fasterxml.jackson.databind.annotation.JsonAppend;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import system.model.Book;
import system.service.BookService;

import java.util.List;

@Controller
public class BookController {
    private BookService bookService;

    @Autowired(required = true)
    @Qualifier(value = "bookService")
    public void setBookService(BookService bookService) {
        this.bookService = bookService;
    }

    @RequestMapping(value = "books", method = RequestMethod.GET)
    public String listBooks(Model model){
        model.addAttribute("book", new Book());
        model.addAttribute("listBooks", this.bookService.listBooks());

        return "books";
    }

//    @RequestMapping(value = "/books/add", method = RequestMethod.POST)
//    public void addBook(@ModelAttribute("book") Book book) {
//
//        if (book.getId() == 0) {
//            this.bookService.addBook(book);
//        } else {
//            this.bookService.updateBook(book);
//        }
//
//    }


    @RequestMapping(value = "/books/add", method = RequestMethod.POST)
    @ResponseBody public String addBook(@ModelAttribute("book") Book book) {
        if (book.getId() == 0) {
            this.bookService.addBook(book);
        } else {
            this.bookService.updateBook(book);
        }
        return "{\"id\":1,\"bookTitle\":\"First Book\",\"bookAuthor\":\"First Author\",\"price\":30000}";
    }

    @RequestMapping(value = "/books/check", method = RequestMethod.GET)
    public @ResponseBody
    String checkUser() {

        return "invalid";
    }


    @RequestMapping("/remove-book/{id}")
    public String removeBook(@PathVariable("id") int id){
        this.bookService.removeBook(id);

        return "redirect:/books";
    }

    @RequestMapping("edit-book/{id}")
    public String editBook(@PathVariable("id") int id, Model model){
        model.addAttribute("book", this.bookService.getBookById(id));
        model.addAttribute("listBooks", this.bookService.listBooks());

        return "books";
    }

    @RequestMapping("bookdata/{id}")
    public String bookData(@PathVariable("id") int id, Model model){
        model.addAttribute("book", this.bookService.getBookById(id));

        return "bookdata";
    }

    // формат для JSON
    @RequestMapping(value = "/json/books", method = RequestMethod.GET)
    public @ResponseBody
    List<Book> listBooks() {

        return bookService.listBooks();

    }

}
