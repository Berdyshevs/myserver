package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Region;
import system.model.Subdivision;
import system.service.RegionService;
import system.service.SubdivisionService;

import java.util.List;

@Controller
public class SubdivisionController {
    private SubdivisionService subdivisionService;

    @Autowired(required = true)
    @Qualifier(value = "subdivisionService")
    public void setSubdivisionService(SubdivisionService subdivisionService) {
        this.subdivisionService = subdivisionService;
    }

//    @RequestMapping(value = "subdivisions", method = RequestMethod.GET)
//    public String listSubdivisions(Model model){
//        model.addAttribute("subdivision", new Subdivision());
//        model.addAttribute("listSubdivisions", this.subdivisionService.listSubdivisions());
//
//        return "subdivisions";
//    }
//
//    @RequestMapping(value = "/subdivisions/add", method = RequestMethod.POST)
//    public String addSubdivision(@ModelAttribute("subdivision") Subdivision subdivision){
//        if(subdivision.getId() == 0){
//            this.subdivisionService.addSubdivision(subdivision);
//        }else {
//            this.subdivisionService.updateSubdivision(subdivision);
//        }
//
//        return "redirect:/subdivisions";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeSubdivision(@PathVariable("id") int id){
//        this.subdivisionService.removeSubdivision(id);
//
//        return "redirect:/subdivisions";
//    }
//
//    @RequestMapping("edit-subdivision/{id}")
//    public String editSubdivision(@PathVariable("id") int id, Model model){
//        model.addAttribute("subdivision", this.subdivisionService.getSubdivisionById(id));
//        model.addAttribute("listSubdivisions", this.subdivisionService.listSubdivisions());
//
//        return "subdivisions";
//    }
//
//    @RequestMapping("subdivisiondata/{id}")
//    public String subdivisionData(@PathVariable("id") int id, Model model){
//        model.addAttribute("subdivision", this.subdivisionService.getSubdivisionById(id));
//
//        return "subdivisiondata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/subdivisions", method = RequestMethod.GET)
    public @ResponseBody

    List<Subdivision> listSubdivisions() {

        return subdivisionService.listSubdivisions();

    }

}
