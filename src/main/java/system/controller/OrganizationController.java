package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Organization;
import system.service.OrganizationService;


import java.util.List;

@Controller
public class OrganizationController {
    private OrganizationService organizationService;

    @Autowired(required = true)
    @Qualifier(value = "organizationService")
    public void setOrganizationService(OrganizationService organizationService) {
        this.organizationService = organizationService;
    }

//    @RequestMapping(value = "organizations", method = RequestMethod.GET)
//    public String listOrganizations(Model model){
//        model.addAttribute("organization", new Organization());
//        model.addAttribute("listOrganizations", this.organizationService.listOrganizations());
//
//        return "organizations";
//    }
//
//    @RequestMapping(value = "/organizations/add", method = RequestMethod.POST)
//    public String addOrganization(@ModelAttribute("organization") Organization organization){
//        if(organization.getId() == 0){
//            this.organizationService.addOrganization(organization);
//        }else {
//            this.organizationService.updateOrganization(organization);
//        }
//
//        return "redirect:/organizations";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeOrganization(@PathVariable("id") int id){
//        this.organizationService.removeOrganization(id);
//
//        return "redirect:/organizations";
//    }
//
//    @RequestMapping("edit-organization/{id}")
//    public String editOrganization(@PathVariable("id") int id, Model model){
//        model.addAttribute("organization", this.organizationService.getOrganizationById(id));
//        model.addAttribute("listOrganizations", this.organizationService.listOrganizations());
//
//        return "organizations";
//    }
//
//    @RequestMapping("organizationdata/{id}")
//    public String organizationData(@PathVariable("id") int id, Model model){
//        model.addAttribute("organization", this.organizationService.getOrganizationById(id));
//
//        return "organizationdata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/organizations", method = RequestMethod.GET)
    public @ResponseBody

    List<Organization> listOrganizations() {

        return organizationService.listOrganizations();

    }

}
