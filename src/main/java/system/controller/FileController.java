package system.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import system.model.UploadedFile;
import system.validator.FileValidator;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@SessionAttributes("filename")
public class FileController {

	@Autowired
	private FileValidator fileValidator;

	private static final Logger logger = LoggerFactory.getLogger(FileController.class);

	@RequestMapping(value = "/uploadFile", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView uploadFile(@ModelAttribute("uploadedFile") UploadedFile uploadedFile, BindingResult result) {// имена параметров - как на форме jsp

		ModelAndView modelAndView = new ModelAndView();

		String fileName = null;

		MultipartFile file = uploadedFile.getFile();
		System.out.println("#прошел 0");
		fileValidator.validate(uploadedFile, result);
		System.out.println("#прошел 1");
		if (result.hasErrors()) {
			modelAndView.setViewName("file");
			System.out.println("#прошел 2");
		} else {

			try {
				byte[] bytes = file.getBytes();

				fileName = file.getOriginalFilename();

				String string_file = fileName.substring(fileName.indexOf(""), fileName.indexOf("."));

				String millis = string_file.substring(fileName.indexOf("_") + 1);

//				System.out.println("MILLSIS > " + millis);
//				Date currentDate = new Date(fileName.substring(fileName.indexOf("_"), fileName.indexOf(".")));
//
//				System.out.println("> " + currentDate);


//				SimpleDateFormat sdf = new SimpleDateFormat("MMM dd,yyyy HH:mm");



//				SimpleDateFormat sdfDay = new SimpleDateFormat("dd");
//				Date day = new Date(Long.valueOf(millis));
//				System.out.println("DAY " + sdfDay.format(resultdate));
//
//				SimpleDateFormat sdfMonth = new SimpleDateFormat("MM");
//				Date month = new Date(Long.valueOf(millis));
//				System.out.println("MONTH " + sdfMonth.format(resultdate));
//
//				SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
//				Date year = new Date(Long.valueOf(millis));
//				System.out.println("YEAR " + sdfYear.format(resultdate));


				SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
				Date resultdate = new Date(Long.valueOf(millis));
				String pathData = "";

				// Сохранение в папки
				if (string_file.contains("min_")) {

					pathData = "#miniatures/" + sdf.format(resultdate);

					System.out.println("MIN: " + fileName.substring(fileName.indexOf(""), fileName.indexOf(".")));

					// ****

					String rootPath = System.getProperty("catalina.home");
					File dir = new File(rootPath + File.separator  + pathData);

					if (!dir.exists()) {
						dir.mkdirs();
					}

					File loadFile = new File(dir.getAbsolutePath() + File.separator + fileName);

					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(loadFile));
					stream.write(bytes);
					stream.flush();
					stream.close();

					logger.info("uploaded: " + loadFile.getAbsolutePath());

					RedirectView redirectView = new RedirectView("fileuploaded");
					redirectView.setStatusCode(HttpStatus.FOUND);
					modelAndView.setView(redirectView);
					modelAndView.addObject("filename", fileName);
					System.out.println("min_прошел 3");

					// ****


				}

				if (string_file.contains("photo_")) {

					pathData = "#photo/" + sdf.format(resultdate);

					System.out.println("PHOTO: " + fileName.substring(fileName.indexOf(""), fileName.indexOf(".")));


					// ****

					String rootPath = System.getProperty("catalina.home");
					File dir = new File(rootPath + File.separator  + pathData);

					if (!dir.exists()) {
						dir.mkdirs();
					}

					File loadFile = new File(dir.getAbsolutePath() + File.separator + fileName);

					BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(loadFile));
					stream.write(bytes);
					stream.flush();
					stream.close();

					logger.info("uploaded: " + loadFile.getAbsolutePath());

					RedirectView redirectView = new RedirectView("fileuploaded");
					redirectView.setStatusCode(HttpStatus.FOUND);
					modelAndView.setView(redirectView);
					modelAndView.addObject("filename", fileName);
					System.out.println("photo_прошел 3");

					// ****
				}


			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println("#Прошел 4(Исключение)");
			}

		}

		return modelAndView;
	}

	@RequestMapping(value = "/fileuploaded", method = RequestMethod.GET)
	public String fileUploaded() {
		return "fileuploaded";
	}


	//для теста загрузки файла с планшета
	@RequestMapping(value = "/file", method = RequestMethod.GET)
		public String files() {
		return "file";
	}

}
