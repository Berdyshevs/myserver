package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Miniature;
import system.model.Region;
import system.service.MiniatureService;
import system.service.RegionService;

import java.util.List;

@Controller
public class MiniatureController {
    private MiniatureService miniatureService;

    @Autowired(required = true)
    @Qualifier(value = "miniatureService")
    public void setMiniatureService(MiniatureService miniatureService) {
        this.miniatureService = miniatureService;
    }

//    @RequestMapping(value = "miniatures", method = RequestMethod.GET)
//    public String listMiniatures(Model model){
//        model.addAttribute("miniature", new Miniature());
//        model.addAttribute("listMiniatures", this.miniatureService.listMiniatures());
//
//        return "miniatures";
//    }
//
//    @RequestMapping(value = "/miniatures/add", method = RequestMethod.POST)
//    public String addMiniature(@ModelAttribute("miniature") Miniature miniature){
//        if(miniature.getId() == 0){
//            this.miniatureService.addMiniature(miniature);
//        }else {
//            this.miniatureService.updateMiniature(miniature);
//        }
//
//        return "redirect:/miniatures";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeMiniature(@PathVariable("id") int id){
//        this.miniatureService.removeMiniature(id);
//
//        return "redirect:/miniatures";
//    }
//
//    @RequestMapping("edit-miniature/{id}")
//    public String editMiniature(@PathVariable("id") int id, Model model){
//        model.addAttribute("miniature", this.miniatureService.getMiniatureById(id));
//        model.addAttribute("listMiniatures", this.miniatureService.listMiniatures());
//
//        return "miniatures";
//    }
//
//    @RequestMapping("miniaturedata/{id}")
//    public String miniatureData(@PathVariable("id") int id, Model model){
//        model.addAttribute("miniature", this.miniatureService.getMiniatureById(id));
//
//        return "miniaturedata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/miniatures", method = RequestMethod.GET)
    public @ResponseBody

    List<Miniature> listMiniatures() {

        return miniatureService.listMiniatures();

    }

}
