package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Room;
import system.service.RoomService;

import java.util.List;

@Controller
public class RoomController {
    private RoomService roomService;

    @Autowired(required = true)
    @Qualifier(value = "roomService")
    public void setRoomService(RoomService roomService) {
        this.roomService = roomService;
    }

//    @RequestMapping(value = "rooms", method = RequestMethod.GET)
//    public String listRooms(Model model){
//        model.addAttribute("room", new Room());
//        model.addAttribute("listRooms", this.roomService.listRooms());
//
//        return "rooms";
//    }
//
//    @RequestMapping(value = "/rooms/add", method = RequestMethod.POST)
//    public String addRoom(@ModelAttribute("room") Room room){
//        if(room.getId() == 0){
//            this.roomService.addRoom(room);
//        }else {
//            this.roomService.updateRoom(room);
//        }
//
//        return "redirect:/rooms";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removeRoom(@PathVariable("id") int id){
//        this.roomService.removeRoom(id);
//
//        return "redirect:/rooms";
//    }
//
//    @RequestMapping("edit-room/{id}")
//    public String editRoom(@PathVariable("id") int id, Model model){
//        model.addAttribute("room", this.roomService.getRoomById(id));
//        model.addAttribute("listRooms", this.roomService.listRooms());
//
//        return "rooms";
//    }
//
//    @RequestMapping("roomdata/{id}")
//    public String roomData(@PathVariable("id") int id, Model model){
//        model.addAttribute("room", this.roomService.getRoomById(id));
//
//        return "roomdata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/rooms", method = RequestMethod.GET)
    public @ResponseBody

    List<Room> listRooms() {

        return roomService.listRooms();

    }

}
