package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.Region;
import system.service.RegionService;

import java.util.List;

@Controller
public class RegionController {
    private RegionService regionService;

    @Autowired(required = true)
    @Qualifier(value = "regionService")
    public void setRegionService(RegionService regionService) {
        this.regionService = regionService;
    }

    @RequestMapping(value = "regions", method = RequestMethod.GET)
    public String listRegions(Model model){
        model.addAttribute("region", new Region());
        model.addAttribute("listRegions", this.regionService.listRegions());

        return "regions";
    }

    @RequestMapping(value = "/regions/add", method = RequestMethod.POST)
    public String addRegion(@ModelAttribute("region") Region region){
        if(region.getId() == 0){
            this.regionService.addRegion(region);
        }else {
            this.regionService.updateRegion(region);
        }

        return "redirect:/regions";
    }

    @RequestMapping("/remove-reg/{id}")
    public String removeRegion(@PathVariable("id") int id){
        this.regionService.removeRegion(id);

        return "redirect:/regions";
    }

    @RequestMapping("edit-region/{id}")
    public String editRegion(@PathVariable("id") int id, Model model){
        model.addAttribute("region", this.regionService.getRegionById(id));
        model.addAttribute("listRegions", this.regionService.listRegions());

        return "regions";
    }

    @RequestMapping("regiondata/{id}")
    public String regionData(@PathVariable("id") int id, Model model){
        model.addAttribute("region", this.regionService.getRegionById(id));

        return "regiondata";
    }

    // формат для JSON
    @RequestMapping(value = "/json/regions", method = RequestMethod.GET)
    public @ResponseBody

    List<Region> listRegions() {

        return regionService.listRegions();

    }

}
