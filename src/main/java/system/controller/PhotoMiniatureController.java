package system.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import system.model.PhotoMiniature;
import system.model.Region;
import system.service.PhotoMiniatureService;
import system.service.RegionService;

import java.util.List;

@Controller
public class PhotoMiniatureController {
    private PhotoMiniatureService photoMiniatureService;

    @Autowired(required = true)
    @Qualifier(value = "photoMiniatureService")
    public void setPhotoMiniatureService(PhotoMiniatureService photoMiniatureService) {
        this.photoMiniatureService = photoMiniatureService;
    }

//    @RequestMapping(value = "photoMiniatures", method = RequestMethod.GET)
//    public String listPhotoMiniatures(Model model){
//        model.addAttribute("photoMiniature", new PhotoMiniature());
//        model.addAttribute("listPhotoMiniatures", this.photoMiniatureService.listPhotoMiniatures());
//
//        return "photoMiniatures";
//    }
//
//    @RequestMapping(value = "/photoMiniatures/add", method = RequestMethod.POST)
//    public String addPhotoMiniature(@ModelAttribute("photoMiniature") PhotoMiniature photoMiniature){
//        if(photoMiniature.getId() == 0){
//            this.photoMiniatureService.addPhotoMiniature(photoMiniature);
//        }else {
//            this.photoMiniatureService.updatePhotoMiniature(photoMiniature);
//        }
//
//        return "redirect:/photoMiniatures";
//    }
//
//    @RequestMapping("/remove-reg/{id}")
//    public String removePhotoMiniature(@PathVariable("id") int id){
//        this.photoMiniatureService.removePhotoMiniature(id);
//
//        return "redirect:/photoMiniatures";
//    }
//
//    @RequestMapping("edit-photoMiniature/{id}")
//    public String editPhotoMiniature(@PathVariable("id") int id, Model model){
//        model.addAttribute("photoMiniature", this.photoMiniatureService.getPhotoMiniatureById(id));
//        model.addAttribute("listPhotoMiniatures", this.photoMiniatureService.listPhotoMiniatures());
//
//        return "photoMiniatures";
//    }
//
//    @RequestMapping("photoMiniaturedata/{id}")
//    public String photoMiniatureData(@PathVariable("id") int id, Model model){
//        model.addAttribute("photoMiniature", this.photoMiniatureService.getPhotoMiniatureById(id));
//
//        return "photoMiniaturedata";
//    }

    // формат для JSON
    @RequestMapping(value = "/json/photominiatures", method = RequestMethod.GET)
    public @ResponseBody

    List<PhotoMiniature> listPhotoMiniatures() {

        return photoMiniatureService.listPhotoMiniatures();

    }

}
