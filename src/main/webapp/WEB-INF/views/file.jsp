<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<c:set var="contextPath" value="${pageContext.request.contextPath}"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!DOCTYPE html>
<html lang="en">
<%--<head>--%>
    <%--<meta charset="utf-8">--%>
    <%--<meta http-equiv="X-UA-Compatible" content="IE=edge">--%>
    <%--<meta name="viewport" content="width=device-width, initial-scale=1">--%>
    <%--<meta name="description" content="">--%>
    <%--<meta name="author" content="">--%>
    <%--<title>FileDonwload</title>--%>

<%--</head>--%>

<%--<br/>--%>

<form:form method="post" enctype="multipart/form-data" modelAttribute="uploadedFile" action="uploadFile">

    <table>
        <tr>
            <td></td>
            <td><input type="file" name="file" /></td>
            <td style="color: red; font-style: italic;">
                <form:errors path="file" /></td>
        </tr>
        <tr>
            <td></td>
            <td><input type="submit" value="Загрузить" /></td>
            <td></td>
        </tr>

    </table>
  
</form:form>

</html>